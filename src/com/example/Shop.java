package com.example;

public  class Shop implements interfaceCreateBill {
    float tea =15;
    int teaQty=0;
    float teaDiscount=((5.0f/100)*15);
    float coffee =20;
    int coffeeQty=0;
    float coffeeDiscount=((3.0f/100)*20);
    float puff =20;
    int puffQty=0;
    float puffDiscount=((10.0f/100)*20);
    float samosa =15;
    int samosaQty=0;
    float samosaDiscount=((20.0f/100)*15);
    float bun =10;
    int bunQty=0;
    float bunDiscount=((25.0f/100)*10);

    public Shop(int teaQty, int coffeeQty, int puffQty, int samosaQty, int bunQty) {
        this.teaQty = teaQty;
        this.coffeeQty = coffeeQty;
        this.puffQty = puffQty;
        this.samosaQty = samosaQty;
        this.bunQty = bunQty;
    }

    @Override
    public float bill() {
        System.out.println("total tea price= "+teaQty*tea+" discount= "+teaQty*teaDiscount+" = "+((teaQty*tea)-(teaQty*teaDiscount)));
        System.out.println("total coffee price= "+coffeeQty*coffee+" discount= "+coffeeQty*coffeeDiscount+" = "+((coffeeQty*coffee)-(coffeeQty*coffeeDiscount)));
        System.out.println("total puff price= "+puffQty*puff+" discount= "+puffQty*puffDiscount+" = "+((puffQty*puff)-(puffQty*puffDiscount)));
        System.out.println("total samosa price= "+samosaQty*samosa+" discount= "+samosaQty*samosaDiscount+" = "+((samosaQty*samosa)-(samosaQty*samosaDiscount)));
        System.out.println("total bun price= "+bunQty*bun+" discount= "+bunQty*bunDiscount+" = "+((bunQty*bun)-(bunQty*bunDiscount)));

        return (((teaQty*tea)-(teaQty*teaDiscount))+((coffeeQty*coffee)-(coffeeQty*coffeeDiscount))+
                ((puffQty*puff)-(puffQty*puffDiscount)) + ((samosaQty*samosa)-(samosaQty*samosaDiscount))+
                ((bunQty*bun)-(bunQty*bunDiscount)));
    }
}


